FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine3.19-amd64 AS publish
COPY . .
RUN dotnet restore
RUN dotnet publish Hello -c Release -o /app

FROM mcr.microsoft.com/dotnet/sdk:8.0-alpine3.19-amd64
WORKDIR /app
EXPOSE 8080
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Hello.dll"]

