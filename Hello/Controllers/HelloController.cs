using Microsoft.AspNetCore.Mvc;

namespace Hello.Controllers;

[ApiController]
[Route("")]
public class HelloController : ControllerBase
{
    [HttpGet("hello-world")]
    public string HelloWorld() => "Hello World";

    [HttpGet("hello-se400")]
    public string HelloSe400() => "Hello Se400";
}

