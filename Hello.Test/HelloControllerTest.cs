using Hello.Controllers;

namespace Hello.Test;

public class HelloControllerTest
{
    [Fact]
    public void CheckNotNull()
    {
        var controller = new HelloController();
        Assert.NotNull(controller);
    }

    [Fact]
    public void CheckHelloWorld()
    {
        var controller = new HelloController();
        string result = controller.HelloWorld();
        Assert.Equal("Hello World", result);
    }
}
